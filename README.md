# omniva-frontend

## This is React + Redux application

Environment variables stored in .env
Default backend API URL: http://localhost:8080/api   
Default frontend app running port 3000

## How to run application

Install dependencies
```
npm install
```

Go to package.json file and choose one of the commands

To start
```
npm run start
```
To build
```
npm run build
```
To run tests (no tests are available)
```
npm run test
```
To eject
```
npm run eject
```