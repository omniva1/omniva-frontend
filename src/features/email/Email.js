import React, { useState } from 'react';
import styles from './Email.module.css';
import { useDispatch, useSelector } from 'react-redux';
import { emailAdd } from './emailSlice.js';

const EmailForm = () => {

    const dispatch = useDispatch()

    const emailObject = useSelector(state => state.email.emailObject);

    const [emailData, setEmailData] = useState(emailObject)
    const {receiver, subject, message} = emailData;

    const validateData = () => {
        if (!emailData.receiver) {
            alert('Empty receiver')
            return false;
        } else if (!emailData.subject) {
            alert('Empty subject')
            return false;
        }
        return true;
    }

    const handleChange = (event) => {
        const {name, value} = event.target;
        setEmailData((prevData) => ({...prevData, [name]: value}))
    }

    const addEmail = (event) => {
        event.preventDefault();
        if (validateData()) {
            dispatch(emailAdd(emailData))
        }
    }

    return (
        <form className={styles.form} onSubmit={addEmail}>
            <div className={styles.row}>
                <div className={styles.label}>To:</div>
                <div><input className={styles.input} type="text" name="receiver" value={receiver}
                            onChange={handleChange}/></div>
            </div>
            <div className={styles.row}>
                <div className={styles.label}>Subject:</div>
                <div><input className={styles.input} type="text" name="subject" value={subject}
                            onChange={handleChange}/></div>
            </div>
            <div className={styles.row}>
                <div className={styles.label}>Message:</div>
                <div><textarea className={styles.textarea} name="message" value={message} onChange={handleChange}/>
                </div>
            </div>

            <input type="submit" value="Submit"/>
        </form>
    );
}

export default EmailForm;