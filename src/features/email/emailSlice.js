import { createSlice } from '@reduxjs/toolkit'
import { saveEmail } from './emailApi.js';

const initialState = {
    emailObject: {
        receiver: '',
        subject: '',
        message: ''
    }
}

export const emailSlice = createSlice({
    name: 'email',
    initialState,
    reducers: {
        emailAdd: (state, action) => {
            handleEmail(action.payload)
                .then(() => alert('Email saved successfully!!'))
        }
    },
})
export const { emailAdd, clearData } = emailSlice.actions

export function handleEmail(data) {
    return saveEmail(data)
        .catch(err => {
            alert('Oops, there is error...')
            throw err
        })
}

export default emailSlice.reducer