import axios from 'axios';

export async function saveEmail(data) {
    await axios.post(`${process.env.REACT_APP_API_URL}/email/add_email`, data)
        .then(res => {
            return res
        })
        .catch(err => {
            throw err
        })

}
