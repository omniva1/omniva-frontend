import React from 'react';
import './App.css';
import EmailForm from './features/email/Email.js';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <EmailForm />
      </header>
    </div>
  );
}

export default App;
